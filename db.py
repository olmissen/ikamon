# DB.py
# This file consists of all DB related methods
# The idea is to create a GameDb object that holds all relevant methods

# for database
import sqlite3
# for path methods like testing if file exists 
import os
# for password encryption
import bcrypt

def yes_or_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.
    
    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is one of True("yes") or False("no").
    """
    valid = {"yes":True,   "y":True,  "ye":True,
             "no":False,     "n":False}
    if default == None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while 1:
        print(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return default
        elif choice in valid.keys():
            return valid[choice]
        else:
            print("Please respond with 'yes' or 'no' "\
                             "(or 'y' or 'n').\n")

#this is very simple manuel testing
def test_method():
	if (yes_or_no("Do you want to create a new user?", default="no")):
		DB.new_player()
	#testing if validation work
	print("PLEASE LOG IN")
	user = input("username: ")
	password = input("password: ")
	if DB.validate_user(user, password):
		DB.list_users()
	else:
		print("username or pasword is wrong")
	if yes_or_no("One more time?", default="yes"):
		test_method()
	else:
		DB.close_connection()

#Our main game database class
class DB:

	# Initialise method in class. 
	# Self refers to the class itself
	def __init__(self):
		database_filename = "game.db"
		self.conn = sqlite3.connect(database_filename)
		self.c = self.conn.cursor()
		print("Opened database successfully")
		#create usertable
		self.c.execute('''CREATE TABLE IF NOT EXISTS user
				      (id			INTEGER	PRIMARY KEY	AUTOINCREMENT,
				      username      TEXT    NOT NULL	UNIQUE,
				      email			TEXT	NOT NULL	UNIQUE,
				      password      TEXT    NOT NULL)''')
		# We are keeping the connection open
		# Meybe we sould close and open again repeatedly
		# important on a muilti user setup as we run out of connections
		# self.conn.close()


	def new_player(self):
		print("Creating new user:")
		username = input("Username: ")
		email = input("Email: ")
		password = input("Password: ")
		password_confirmation = input("Confirm Password: ")
		if(password == password_confirmation):
			# Hash a password for the first time, with a randomly-generated salt
			hashed = bcrypt.hashpw(bytes(password, 'utf-8'), bcrypt.gensalt())
			# Check that a unhashed password matches one that has previously been
			#   hashed
			try:
				self.c.execute("INSERT INTO user (username, email, password) VALUES (?, ?, ?)", (username, email, hashed))
				self.conn.commit()
			except sqlite3.IntegrityError:
				print("User exists \n Please try again")
				self.new_player()



	def close_connection(self):
		self.conn.close()


	def validate_user(self, username, password):
		self.c.execute("SELECT password FROM user WHERE (username=?)", ([username]))
		hashed = self.c.fetchone()[0]
		password = bytes(password, 'utf-8')
		return bcrypt.checkpw(password, hashed)

	def list_users(self):
		#padding when printing usernames and email
		padding = 2
		
		#get usernames and emails from db
		self.c.execute("SELECT username, email FROM user")
		userlist = self.c.fetchall()
		
		#split into a users and email tuple
		users = [usertuple[0] for usertuple in userlist]
		email_addresses = [usertuple[1] for usertuple in userlist]
		
		#get max length of username and email
		max_username_length = len(max(users, key=len)) + padding
		max_email_address_length = len(max(email_addresses, key=len)) + padding
		
		#print username and email centeret in a string with the max length of both respectively
		print(str.center("USER", max_username_length) + "|" + str.center("EMAIL",max_email_address_length))
		print("-" * (max_username_length + max_email_address_length + 1))
		for user, email in userlist:
			print(str.center(user, max_username_length) + "|" + str.center(email, max_email_address_length))


DB = DB()
test_method()
